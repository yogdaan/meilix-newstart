#!/bin/bash

sudo apt install \
    debootstrap \
    grub-efi-amd64-bin \
    grub-pc-bin \
    mtools \
    ovmf \
    squashfs-tools \
    xorriso \
    zsync\
    realpath
#    qemu-efi \
#    qemu-kvm \

